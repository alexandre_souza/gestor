import { GestorPage } from './app.po';

describe('gestor App', () => {
  let page: GestorPage;

  beforeEach(() => {
    page = new GestorPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
