import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EntidadeDashboardComponent } from './entidades/entidade-dashboard/entidade-dashboard.component';
import { EntidadeListComponent } from './entidades/entidade-list/entidade-list.component';
import { EntidadeDetailsComponent } from './entidades/entidade-details/entidade-details.component';
import { EntidadeDeleteComponent } from './entidades/entidade-delete/entidade-delete.component';
import { EntidadeEditComponent } from './entidades/entidade-edit/entidade-edit.component';
import { EnderecoListComponent } from './endereco/endereco-list/endereco-list.component';

const routes: Routes = [
  { path: 'entidades/list', component: EntidadeListComponent},  
  { path: 'dashboard', component: EntidadeDashboardComponent},
  { path: 'entidades/edit/:id', component: EntidadeEditComponent},
  { path: 'entidades/edit', component: EntidadeEditComponent},
  { path: 'entidades/delete/:id', component: EntidadeDeleteComponent},

  { path: 'enderecos/list', component: EnderecoListComponent}, 

  { path: '', pathMatch: 'full', redirectTo: 'entidades/list'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
