import { Component, OnInit } from '@angular/core';
import { EntidadesService } from '../entidades.service';
import { Entidade } from '../model/entidade';

@Component({
  selector: 'app-entidade-list',
  templateUrl: './entidade-list.component.html',
  styleUrls: ['./entidade-list.component.css']
})
export class EntidadeListComponent implements OnInit {

  entidades: Entidade[];  
  
  titulo: string = 'Entidades';

  constructor(private entidadesService: EntidadesService) {} 

  ngOnInit() {
    this.getEntidades();
  }


  getEntidades(): void{
    this.entidadesService.getEntidades()
    .subscribe(entidades => this.entidades = entidades);
  }

}
