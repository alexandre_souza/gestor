import { Component, OnInit } from '@angular/core';
import { EntidadesService } from '../entidades.service';
import { Entidade } from '../model/entidade';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-entidade-details',
  templateUrl: './entidade-details.component.html',
  styleUrls: ['./entidade-details.component.css']
})
export class EntidadeDetailsComponent implements OnInit {

  entidade: Entidade; 

  formulario: FormGroup;

  constructor(  
    private route: ActivatedRoute,
    private entidadesService: EntidadesService,
    private location: Location,
    private formBuilder: FormBuilder
  ) { }

  ngOnInit() {

    this.formulario = this.formBuilder.group({
      nome: [null],
      apelido: [null]
    });  
  }

  getEntidade(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.entidadesService.getEntidade(id)
      .subscribe(entidade => this.entidade = entidade);
  }

  onSubmit(){
    this.entidadesService.addEntidade(this.formulario.value)
    .subscribe((dados) => {
      console.log(dados);
      this.formulario.reset;
      this.goBack();  
    },
    (error: any) => alert('erro'));
  }  

  goBack(): void {
    this.location.back();
  } 
}
