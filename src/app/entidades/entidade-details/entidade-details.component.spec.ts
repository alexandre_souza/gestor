import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EntidadeDetailsComponent } from './entidade-details.component';

describe('EntidadeDetailsComponent', () => {
  let component: EntidadeDetailsComponent;
  let fixture: ComponentFixture<EntidadeDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EntidadeDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EntidadeDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
