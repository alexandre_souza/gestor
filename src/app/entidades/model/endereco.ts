export class Endereco {
    constructor(
        public situacao: string,
        public tipoendereco: number,
        public tipologradouro: string,
        public logradouro: string,
        public numero: string,
        public complemento: string,
        public bairro: string,
        public cep: string,
        public cepestrangeiro: string,
        public uf: string,
        public ufibge: string,
        public cidade: string,
        public cidadeibge: string,
        public pais: string,
        public paisibge: string,
        ){}
}