import { Endereco } from "./endereco";

export class Entidade {
    constructor(
        public id: number,
        public codigofilial: number,
        public situacao: number,
        public apelido: string,
        public nome: string,
        public nomemae: string,
        public nomepai: string,
        public fisicajuridica: string,
        public datanascimento: Date,
        public sexo: string,
        public estadocivil: string,
        public nacionalidade: string,
        public naturalidade: string,
        public tratadocomo: string,
        public profissao: string,
        public cpfcnpj: string,
        public ie: string,
        public iedatainscricao: Date,
        public im: string,
        public imdatainscricao: Date,
        public rg: string,
        public rgtipo: string,
        public rgdataemissao: Date,
        public cnh: string,
        public cnhvalidade: Date,
        public inscricaoestadual: string,
        public observacao: string,
        public endereco:  Endereco[]
        ){}
}