import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EntidadeDashboardComponent } from './entidade-dashboard.component';

describe('EntidadeDashboardComponent', () => {
  let component: EntidadeDashboardComponent;
  let fixture: ComponentFixture<EntidadeDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EntidadeDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EntidadeDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
