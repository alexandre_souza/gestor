import { Component, OnInit } from '@angular/core';
import { EntidadesService } from '../entidades.service';
import { Entidade } from '../model/entidade';

@Component({
  selector: 'app-entidade-dashboard',
  templateUrl: './entidade-dashboard.component.html',
  styleUrls: ['./entidade-dashboard.component.css']
})
export class EntidadeDashboardComponent implements OnInit {

  entidades: Entidade[];  
  
    titulo: string = 'Entidades';
  
    constructor(private entidadesService: EntidadesService) {} 
  
    ngOnInit() {
      this.getEntidades();
    }
  
    getEntidades(): void{
      this.entidadesService.getEntidades()
      .subscribe(entidades => this.entidades = entidades);
    }

}
