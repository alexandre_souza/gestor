import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { RequestOptions } from '@angular/http';
 
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { catchError, map, tap } from 'rxjs/operators';
 
import { Entidade } from './model/entidade';
import { FuncoesService } from './../funcoes/funcoes.service';
 
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};
 
@Injectable()
export class EntidadesService {
 
  private EntidadeesUrl = 'http://localhost:8000/api/entidade';  // URL to web api
 
  constructor(
    private http: HttpClient,
    private funcoesService: FuncoesService) { }
 
  /** GET Entidadees from the server */
  getEntidades (): Observable<Entidade[]> {
    return this.http.get<Entidade[]>(this.EntidadeesUrl)
      .pipe(
        tap(Entidadees => this.log(`fetched Entidadees`)),
        catchError(this.handleError('getEntidades', []))
      );
  }
 
  /** GET Entidade by id. Return `undefined` when id not found */
  getEntidadeNo404<Data>(id: number): Observable<Entidade> {
    const url = `${this.EntidadeesUrl}/?id=${id}`;
    return this.http.get<Entidade[]>(url)
      .pipe(
        map(Entidadees => Entidadees[0]), // returns a {0|1} element array
        tap(h => {
          const outcome = h ? `fetched` : `did not find`;
          this.log(`${outcome} Entidade id=${id}`);
        }),
        catchError(this.handleError<Entidade>(`getEntidade id=${id}`))
      );
  }
 
  /** GET Entidade by id. Will 404 if id not found */
  getEntidade(id: number): Observable<Entidade> {
    const url = `${this.EntidadeesUrl}/${id}`;
    return this.http.get<Entidade>(url).pipe(
      tap(_ => this.log(`fetched Entidade id=${id}`)),
      catchError(this.handleError<Entidade>(`getEntidade id=${id}`))
    );
  }
 
  /* GET Entidadees whose name contains search term */
  searchEntidadees(term: string): Observable<Entidade[]> {
    if (!term.trim()) {
      // if not search term, return empty Entidade array.
      return of([]);
    }
    return this.http.get<Entidade[]>(`api/Entidadees/?name=${term}`).pipe(
      tap(_ => this.log(`found Entidadees matching "${term}"`)),
      catchError(this.handleError<Entidade[]>('searchEntidadees', []))
    );
  }
 
  //////// Save methods //////////
 
  /** POST: add a new Entidade to the server */
  addEntidade (Entidade: Entidade): Observable<Entidade> {
    return this.http.post<Entidade>(this.EntidadeesUrl, Entidade, httpOptions).pipe(
      tap((Entidade: Entidade) => this.log(`added Entidade w/ id=${Entidade.id}`)),
      catchError(this.handleError<Entidade>('addEntidade'))
    );
  }

 
  /** DELETE: delete the Entidade from the server */
  deleteEntidade (Entidade: Entidade | number): Observable<Entidade> {
    const id = typeof Entidade === 'number' ? Entidade : Entidade.id;
    const url = `${this.EntidadeesUrl}/${id}`;
 
    return this.http.delete<Entidade>(url, httpOptions).pipe(
      tap(_ => this.log(`deleted Entidade id=${id}`)),
      catchError(this.handleError<Entidade>('deleteEntidade'))
    );
  }
 
  /** PUT: update the Entidade on the server */
  updateEntidade(entidade: Entidade):Observable<number> {
    const url = `${this.EntidadeesUrl}/${entidade.id}`;
    return this.http.put(url, entidade, httpOptions).pipe(
      tap(_ => this.log(`updated Entidade id=${Entidade}`)),
      catchError(this.handleError<any>('updateEntidade'))
    );
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
 
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
 
      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);
 
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
 
  /** Log a EntidadeService message with the MessageService */
  private log(message: string) {
    this.funcoesService.messageAdd('EntidadeService: ' + message);
  }
}