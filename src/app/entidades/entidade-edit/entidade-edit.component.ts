import { Component, OnInit, Input } from '@angular/core';
import { Entidade } from '../model/entidade';
import { EntidadesService } from '../entidades.service';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { FormControl, FormBuilder, FormGroup, Validators, FormArray} from '@angular/forms';
import { Console } from '@angular/core/src/console';
import { EnderecoListComponent } from '../../endereco/endereco-list/endereco-list.component';
import { Endereco } from '../model/endereco';


@Component({
  selector: 'app-entidade-edit',
  templateUrl: './entidade-edit.component.html',
  entryComponents: [EnderecoListComponent],
  styleUrls: ['./entidade-edit.component.css']
})
export class EntidadeEditComponent implements OnInit {

  entidade: Entidade; 
  id: number;
  statusCode: number;
  
  formEntidade: FormGroup;

  originalRequest: string;
  constructor(  
    private route: ActivatedRoute,
    private entidadesService: EntidadesService,
    private location: Location,
    private formBuilder: FormBuilder
  ) { 
    this.createForm();
  }

  ngOnInit() {
    this.id = +this.route.snapshot.paramMap.get('id');

    if (this.id != 0) {
      this.originalRequest = 'Alteração';
      this.entidadesService.getEntidade(this.id)
        .subscribe(entidade => { 
              this.formEntidade.patchValue(entidade);            
              this.setEnderecos(entidade.endereco);
            },
            errorCode =>  this.statusCode = errorCode);   
    } else {
      this.originalRequest = 'Inclusão';
    }
  }

  get enderecos(): FormArray {
    return this.formEntidade.get('enderecos') as FormArray;
  };

  setEnderecos(enderecos: Endereco[]) {
    const enderecosFGs = enderecos.map(enderecosMap => this.formBuilder.group(enderecosMap));
    const enderecosFormArray = this.formBuilder.array(enderecosFGs); 
    this.formEntidade.setControl('enderecos', enderecosFormArray);
  }

  onSubmit(){
    let entidade = this.formEntidade.value;
    if (this.id != 0) {
      entidade.id = this.id;
      this.entidadesService.updateEntidade(entidade)
      .subscribe((dados) => {
        this.formEntidade.reset;
        this.goBack();  
      },
      (error: any) => alert('erro')); 

    } else {
     console.log('formEntidade: ' + this.formEntidade);
      this.entidadesService.addEntidade(this.formEntidade.value)
      .subscribe((dados) => {
        this.formEntidade.reset;
        this.goBack();  
      },
      (error: any) => alert('erro'));
    } 
  }  

  goBack(): void {
    this.location.back();
  } 


  createForm() {
    this.formEntidade = this.formBuilder.group({
      nome: [null, Validators.required],
      apelido: [null],
      codigofilial: [null],
      situacao: [null],
      nomemae: [null],
      nomepai: [null],
      fisicajuridica: [null],
      datanascimento: [null],
      sexo: [null],
      estadocivil: [null],
      nacionalidade: [null],
      naturalidade: [null],
      tratadocomo: [null],
      profissao: [null],
      cpfcnpj: [null],
      ie: [null],
      iedatainscricao: [null],
      im: [null],
      imdatainscricao: [null],
      rg: [null],
      rgtipo: [null],
      rgdataemissao: [null],
      cnh: [null],
      cnhvalidade: [null],
      inscricaoestadual: [null],
      observacao: [null],
      enderecos: this.formBuilder.array([])
  });
}

}
