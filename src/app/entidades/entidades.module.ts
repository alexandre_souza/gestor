import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppRoutingModule } from '../app-routing.module';
import { ReactiveFormsModule } from '@angular/forms';
import { CollapsibleModule } from 'angular2-collapsible';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { EntidadesService } from './entidades.service';


import { EntidadeDashboardComponent } from './entidade-dashboard/entidade-dashboard.component';
import { EntidadeDetailsComponent } from './entidade-details/entidade-details.component';
import { EntidadeListComponent } from './entidade-list/entidade-list.component';
import { EntidadeDeleteComponent } from './entidade-delete/entidade-delete.component';
import { EntidadeEditComponent } from './entidade-edit/entidade-edit.component';
import { EnderecoListComponent } from '../endereco/endereco-list/endereco-list.component';
import { EnderecoModule } from '../endereco/endereco.module';

import { AppComponent } from '../app.component';



@NgModule({
  imports: [
    CommonModule,
    AppRoutingModule,
    EnderecoModule,
    ReactiveFormsModule,
    CollapsibleModule,
    BrowserAnimationsModule
  ],
  declarations: [
    EntidadeDashboardComponent, 
    EntidadeListComponent,
    EntidadeEditComponent,
    EntidadeDetailsComponent,
    EntidadeDeleteComponent
  ],
  providers : [
    EntidadesService
  ]
})
export class EntidadesModule { }
