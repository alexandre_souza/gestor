import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EntidadeDeleteComponent } from './entidade-delete.component';

describe('EntidadeDeleteComponent', () => {
  let component: EntidadeDeleteComponent;
  let fixture: ComponentFixture<EntidadeDeleteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EntidadeDeleteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EntidadeDeleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
