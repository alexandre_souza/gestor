import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { EntidadesService } from '../entidades.service';
import { Entidade } from '../model/entidade';


@Component({
  selector: 'app-entidade-delete',
  templateUrl: './entidade-delete.component.html',
  styleUrls: ['./entidade-delete.component.css']
})
export class EntidadeDeleteComponent implements OnInit {

  entidade: Entidade; 
  constructor(
    private route: ActivatedRoute,
    private entidadesService: EntidadesService
  ) { }

  ngOnInit() {
    this.deleteEntidades();
  }

  deleteEntidades(): void{
    const id = +this.route.snapshot.paramMap.get('id');
    this.entidadesService.deleteEntidade(id)
    .subscribe(entidades => this.entidade = entidades);
  }

}
