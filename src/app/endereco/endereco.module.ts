import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppRoutingModule } from '../app-routing.module';
import { ReactiveFormsModule } from '@angular/forms';

import { EnderecoListComponent } from './endereco-list/endereco-list.component';
import { EnderecosService } from './enderecos.service';
import { EnderecoEditComponent } from './endereco-edit/endereco-edit.component';

@NgModule({
  imports: [
    CommonModule,
    AppRoutingModule,
    ReactiveFormsModule
  ],
  exports: [
    EnderecoListComponent,
    EnderecoEditComponent   
  ],
  declarations: [
    EnderecoListComponent,
    EnderecoEditComponent
  ],
  providers: [
    EnderecosService
  ]
})
export class EnderecoModule { }
