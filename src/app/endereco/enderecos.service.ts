import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { RequestOptions } from '@angular/http';
 
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { catchError, map, tap } from 'rxjs/operators';

import { Endereco } from '../entidades/model/endereco';
import { FuncoesService } from '../funcoes/funcoes.service';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class EnderecosService {

  private EnderecosUrl = 'http://localhost:8000/api/endereco';  // URL to web api

  constructor(
    private http: HttpClient,
    private funcoesService: FuncoesService) { }

  /** GET Enderecos from the server */
  getEnderecos(): Observable<Endereco[]> {
    return this.http.get<Endereco[]>(this.EnderecosUrl)
      .pipe(
        tap(Entidadees => this.log(`fetched Entidadees`)),
        catchError(this.handleError('getEntidades', []))
      );
  }
  
    /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
 
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
 
      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);
 
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
 
  /** Log a EntidadeService message with the MessageService */
  private log(message: string) {
    this.funcoesService.messageAdd('EnderecosService: ' + message);
  }

}
