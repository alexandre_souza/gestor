import { Component, OnInit } from '@angular/core';
import { Endereco } from '../../entidades/model/endereco';
import { EnderecosService } from '../enderecos.service';

@Component({
  selector: 'app-endereco-list',
  templateUrl: './endereco-list.component.html',
  styleUrls: ['./endereco-list.component.css']
})
export class EnderecoListComponent implements OnInit {

  enderecos: Endereco[];

  titulo: string = 'Endereços';

  constructor(private enderecosService: EnderecosService) { }

  ngOnInit() {
    this.getEnderecos();
  }

  getEnderecos(): void{
    this.enderecosService.getEnderecos()
    .subscribe(enderecos => this.enderecos = enderecos);
  }

}
