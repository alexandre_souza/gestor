import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, NgForm, ReactiveFormsModule} from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { EntidadesModule } from "./entidades/entidades.module";
import { HttpClientModule } from '@angular/common/http';
import { FuncoesModule } from './funcoes/funcoes.module';
import { FuncoesService } from './funcoes/funcoes.service';

import { EnderecoModule } from './endereco/endereco.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    ReactiveFormsModule,
    AppRoutingModule,
    EntidadesModule,
    EnderecoModule,
    HttpClientModule,
    FuncoesModule
  ],
  providers: [FuncoesService],
  bootstrap: [AppComponent]
})
export class AppModule { }
