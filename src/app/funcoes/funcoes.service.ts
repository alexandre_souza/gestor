import { Injectable } from '@angular/core';

@Injectable()
export class FuncoesService {

  constructor() { }

  messages: string[] = [];
  
   messageAdd(message: string) {
     this.messages.push(message);
   }
  
   messageClear() {
     this.messages.length = 0;
   }

}
