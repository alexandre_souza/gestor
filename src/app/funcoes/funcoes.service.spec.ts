import { TestBed, inject } from '@angular/core/testing';

import { FuncoesService } from './funcoes.service';

describe('FuncoesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FuncoesService]
    });
  });

  it('should be created', inject([FuncoesService], (service: FuncoesService) => {
    expect(service).toBeTruthy();
  }));
});
